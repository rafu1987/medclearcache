<?php
namespace RZ\Medclearcache\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Raphael Zschorsch <rafu1987@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 * @package Medclearcache
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class CacheItem implements \TYPO3\CMS\Backend\Toolbar\ClearCacheActionsHookInterface {

    /**
     * Adds the option to clear all the caches in the back-end clear cache menu.
     *
     * @param array $a_cacheActions
     * @param array $a_optionValues
     * @return void
     * @see typo3/interfaces/backend_cacheActionsHook#manipulateCacheActions($cacheActions, $optionValues)
     */
    public function manipulateCacheActions(&$a_cacheActions, &$a_optionValues) {
        if($GLOBALS['BE_USER']->isAdmin() || $GLOBALS['BE_USER']->getTSConfigVal('options.clearCache.medclearcache')) {
            $s_title = $GLOBALS['LANG']->sL('LLL:EXT:medclearcache/Resources/Private/Language/locallang.xlf:rm.clearCacheMenu_medClearCache', true);
            $s_imagePath = ExtensionManagementUtility::extRelPath('medclearcache').'Resources/Public/Images/';
            if(strpos($s_imagePath,'typo3conf') !== false) $s_imagePath = '../'.$s_imagePath;
            $a_cacheActions[] = array(
                'id'    => 'medclearcache',
                'title' => $s_title,
                'href' => 'ajax.php?ajaxID=ClearCache::clear',
                'icon'  => '<img src="'.$s_imagePath.'Backend.png" title="'.$s_title.'" alt="'.$s_title.'" />',
            );
            $a_optionValues[] = 'clearCacheMed';
        }
    }

}