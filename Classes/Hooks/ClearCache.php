<?php

namespace RZ\Medclearcache\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Raphael Zschorsch <rafu1987@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 * @package Medclearcache
 */

class ClearCache {

    /**
     * Clears all caches
     *
     * @return  void
     */
    public function clear() {
        $cacheDir = PATH_site.'typo3temp/Cache';

        self::rrmdir($cacheDir);

        // Remove temp files
        $files = glob(PATH_site.'typo3conf/temp_CACHED_*.php');
        array_walk($files, function ($file) {
            unlink($file);
        });

        // Remove temp razor files
        if(is_dir(PATH_site.'typo3temp/razor')) {
            $files = glob(PATH_site.'typo3temp/razor/Temp/*.razor');
            array_walk($files, function ($file) {
                unlink($file);
            });

            unlink(PATH_site.'typo3temp/razor/razor.less');
        }

        // Remove temp t3med files
        if(is_dir(PATH_site.'typo3temp/t3med')) {
            $files = glob(PATH_site.'typo3temp/t3med/Temp/*.t3med');
            array_walk($files, function ($file) {
                unlink($file);
            });

            unlink(PATH_site.'typo3temp/t3med/t3med.less');
        }

        // Clear cf_* tables
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_hash;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_hash_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_pages;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_pagesection;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_pagesection_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_pages_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_rootline;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_cache_rootline_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_datamapfactory_datamap;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_datamapfactory_datamap_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_object;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_object_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_reflection;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_reflection_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_typo3dbbackend_queries;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_typo3dbbackend_queries_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_typo3dbbackend_tablecolumns;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_extbase_typo3dbbackend_tablecolumns_tags;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_workspaces_cache;');
        $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE cf_workspaces_cache_tags;');
    }

    public function rrmdir($dir) { 
        if (is_dir($dir)) { 
            $objects = scandir($dir); 
            foreach ($objects as $object) { 
                if ($object != "." && $object != "..") { 
                    if (filetype($dir."/".$object) == "dir") self::rrmdir($dir."/".$object); else unlink($dir."/".$object); 
                } 
            } 
            reset($objects); 
            rmdir($dir); 
        } 
    }

}