<?php
if(!defined('TYPO3_MODE')){
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['additionalBackendItems']['cacheActions'][] = 'RZ\Medclearcache\Hooks\CacheItem';
$TYPO3_CONF_VARS['BE']['AJAX']['ClearCache::clear'] = 'RZ\Medclearcache\Hooks\ClearCache->clear';