<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "medclearcache".
 *
 * Auto generated 24-03-2014 21:35
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Clear cache',
	'description' => 'Remove typo3temp/Cache/ folder and perform other cache deletions',
	'category' => 'be',
    'author' => 'Raphael Zschorsch',
    'author_email' => 'rafu1987@gmail.com',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => true,
	'version' => '0.1.1',
	'constraints' => array(
		'depends' => array(
		  'typo3' => '6.2.0-6.2.99',
		),
		'conflicts' => array(
		
        ),
		'suggests' => array(

		),
	),
);